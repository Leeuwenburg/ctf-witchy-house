#!/usr/bin/python3
# HINT: https://dn.ht/picklecat/

import os
import pickle


PICKLE_PATH = '/tmp/'
fname = PICKLE_PATH + 'beatrix.pickle'

if os.path.exists(fname):
    # Load up the cached command matrix
    command_matrix = pickle.load(open(fname, 'rb'))

else:

    # Dump out a template file if nothing exists
    base_path = '/home/Beatrix'
    shell_command = 'ls'

    command_matrix = [
        (base_path, shell_command)
    ]

    pickle.dump(command_matrix, open(fname, 'wb'))
    pickle.dumps(command_matrix)

# Execute the command matrix
for base_path, shell_command in command_matrix:
    os.chdir(base_path)
    os.system(shell_command)