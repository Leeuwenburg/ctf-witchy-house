import random

from flask import Flask
from flask import render_template
from flask import request


import os
os.environ['WERKZEUG_DEBUG_PIN'] = 'off'

QUOTES = [
    '"The limits of my language are the limits of my mind. All I know is what I have words for."',
    '''"Don't believe everything you think!"''',
    '"Muahahahahahahaha',
    '"Zagan is coming. Prepare."',
    '"Evanora has the best fashion sense of everyone."'
]

app = Flask(__name__)

@app.route("/")
def hello():
    return "This is Dylan's server. Go away."


@app.route('/warlock666',  methods=['GET', 'POST'])
def special_url():
    args = request.args
    quotestring = QUOTES[0]
    if 'quote' in args:
        quote_index = int(args['quote'])
        quotestring = QUOTES[quote_index]

    print(args)

    return render_template('secret.html',
        quotestring = quotestring,
        next_quote = random.randint(0, len(QUOTES)-1)
    )




if __name__ == "__main__":
    app.run(debug=True)