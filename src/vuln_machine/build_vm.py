#!/usr/bin/python3

'''
Apologies to the sensibilities of good Python developer everywhere, I have not
written this Pythonically.
'''


# Process overview:
# Create new VM from base image
# Initialise the new VM with users etc
#

# def generate_unique_key():
#     print('hi')

import math
import os
import random

def make_token():
    bits = random.getrandbits(128)
    token = str(hex(bits))
    return token

def make_password():
    bits = random.getrandbits(128)
    password = str(hex(bits))
    return password

def make_witches():

    witches = [
        ('Agnes', 'apprentices', make_token(), make_password()),
        ('Beatrix', 'apprentices', make_token(), make_password()),
        ('Cain', 'apprentices', make_token(), make_password()),
        ('Dylan', 'apprentices', make_token(), make_password()),
        ('Evanora', 'apprentices', make_token(), make_password()),
        ('Faustine', 'apprentices', make_token(), make_password()),
        ('Zagan', 'demons', make_token(), make_password())
    ]

    fname = '/vagrant/witch_passwords.txt'
    outfile = open(fname, 'w')
    print(str(('Name', 'group', 'token', 'password')), file=outfile)
    print(str(witches), file=outfile)
    outfile.close()

    for witch in witches:
        make_user(*witch)

def make_users(n):
    '''
    Users have "batched" passwords. Because there's no registration system, we just generate a ton of users
    and assign passwords in groups of ten and rely on the users to sort out the mess. We'll give each table
    a password and 10 users, and they can change the password after logging in.
    '''

    # There's probably a nice list comprehension to be had here
    for i in range(n):
        username = "user%s" % str(i)
        default_group = 'noobs'
        unique_token = make_token()
        password = 'password'

        make_user(username, default_group, unique_token, password)


    # fname = '/vagrant/user_passwords.txt'
    # outfile = open(fname, 'w')
    # print(passwords, file=outfile)
    # outfile.close()


def copy_files(basedir='/vagrant'):
    '''
    Expects a shadow /home set of directories in /vagrant synced folder.
    Copies files out of here into the users home directories on a per-user basis.

    If the machine is getting its files some other way, you can override the base dir
    '''

    for user in ['Agnes', 'Beatrix', 'Cain', 'Dylan']:

        os.system('cp %s/%s/* /home/%s' % (basedir, user, user))
        os.system('chown %s /home/%s/*' % (user, user))
        os.system('chgrp apprentices /home/%s/*' % (user))

def make_user(username, default_group, unique_token, password):
    '''
    Make a new user on the system with a suitable home directory and basic setup.
    '''

    # Set the use up in the system
    os.system('useradd %s' % username)
    os.system('mkdir -p /home/%s' % username)
    os.system('chown %s /home/%s' % (username, username))

    os.system('chgrp %s /home/%s' % (default_group, username))
    os.system('usermod -g %s %s' % (default_group, username))
    os.system('usermod -s /bin/bash %s' % username)
    os.system('echo "%s:%s" | chpasswd' % (username, password))

    # Create tokens and files
    os.system('echo %s > /home/%s/token.txt' % (unique_token, username))
    os.system('chown %s /home/%s/token.txt' % (username, username))

    os.system('chmod -R 700 /home/%s' % (username))
    os.system('chmod 750 /home/%s' % (username))


# def make_dylan_vulnerable():
#
    # os.system('sudo -u Dylan python3 /home/Dylan/simple_server.py')

def make_cain_vulnerable():
    '''
    Set up Cain's crontab to run his vulnerable Python code every 1 minutes.
    '''

    cmd = '(crontab -l 2>/dev/null; echo "*/1 * * * * /home/Cain/python_demo.py") | crontab -u Cain - '
    os.system(cmd)

def make_agnes_vulnerable():
    '''
    This is a completely ridiculous vulnerability but people will still need to scan
    for it so it's something
    '''
    username = 'Agnes'
    password = make_password()
    cmd = 'echo "%s:%s" | chpasswd' % (username, password)
    os.system(cmd)
    os.system('echo %s > /home/Agnes/password.txt' % password)
    os.system('chown Agnes /home/Agnes/password.txt')
    os.system('chmod 755 /home/Agnes/')
    os.system('chmod 666 /home/Agnes/password.txt')

def make_beatrix_vulnerable():
    '''
    This makes Beatrix's web server run setuid, which means it shows all her files and runs with her permissions.

    This allows the user to access her token and information. There is no user takeover.
    '''
    os.system('chmod 4750 /home/Beatrix/run_server.sh')


if __name__ == '__main__':

    cwd = os.getcwd()
    print(cwd)

    copy_files()

    os.system('groupadd apprentices')
    os.system('groupadd noobs')
    os.system('groupadd demons')

    print('Setting up witches and warlocks')
    make_witches()

#     # print('Setting up demon')
#     # make_demon()

    print('Setting up users')
    make_users(50)

    print('setting up vulnerabilities')
    make_agnes_vulnerable()
    make_beatrix_vulnerable()
    make_cain_vulnerable()
    # make_dylan_vulnerable()






