#!/usr/bin/python3
# HINT: https://dn.ht/picklecat/

import os
import os.path
import pickle
import pwd

PICKLE_PATH = '/tmp/'

fname = PICKLE_PATH + 'cain.pickle'

if os.path.exists(fname):
    command_matrix = pickle.load(open(fname, 'r'))


    for base_path, shell_command in command_matrix:
        os.chdir(base_path)
        os.system(shell_command)

else:

    print('Input file not found')
    message = pwd.getpwuid(os.getuid()).pw_name
    outfile = '/tmp/cain.txt'
    f = open(outfile, 'w')
    f.write(message)
    f.close()


os.system('rm %s' % fname)