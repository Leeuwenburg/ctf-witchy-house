'''
Overview of the CTF chat app

All interactions go through the "ctf_wizard" user. The metaphor is that the users are working with the wizard to achieve the mission.

The app is capable of:
  -- Saying "Hurrumph!" on first connection
  -- Saying random things as the wizard user for dramatic effect
  -- Handling token claims, validating correct tokens, and recording the claim
  -- Responding to score queries


For future development:
  -- Every ten minutes, play the "bell" sound and post the answer to the previous challenge
  -- Half-way through each interval, post a hint
  -- Play spooky sounds when the users trip a trap
  -- Conduct a randomly generated conversation among different wizard personas

'''


import os
import time
import re
from slackclient import SlackClient


# constants
RTM_READ_DELAY = 3 # 1 second delay between reading from RTM
CLAIM_TOKEN = "Claim token: "
MENTION_REGEX = "<@(|[WU].+?)>(.*)"

# instantiate Slack client
slack_client = SlackClient(os.environ.get('SLACK_BOT_TOKEN'))

# starterbot's user ID in Slack: value is assigned after the bot starts up
config = {
    'wizard_id': None
}

def timerPlay():

    {
        0: ['spooky_low_tone.mp3'],
        12: ['bell.mp3'],
        22: ['bell.mp3'],
        32: ['bell.mp3'],
        42: ['bell.mp3'],
        52: ['bell.mp3'],
        62: ['bell.mp3'],
    }

def timerSay():

    {
        0: 'intro.txt',
        12: 'Ten_minutes down!',
        22: ['bell.mp3'],
        32: ['bell.mp3'],
        42: ['bell.mp3'],
        52: ['bell.mp3'],
        62: ['bell.mp3'],
    }


def main():

    # os.system('afplay ../sounds/spooky_low_tone.mp3')

    if slack_client.rtm_connect(with_team_state=False):

        # Basic setup
        print("Starter Bot connected and running!")
        # Read bot's user ID by calling Web API method `auth.test`
        wizard_id = slack_client.api_call("auth.test")["user_id"]
        config['wizard_id'] = wizard_id
        print(wizard_id)

        slack_client.api_call(
            "chat.postMessage",
            channel="general",
            text="Hurrrumph! :tada:",
            as_user=True
        )

        # Main loop -- parse, handle, repeat
        while True:
            slack_events = slack_client.rtm_read()
            print('.')
            to_wizard = parse_wizard_commands(slack_events)
            # mention_wizard = parse_wizard_mentions(slack_events)

            # handle_commands(to_wizard)
            # handle_mentions(mention_wizard)

            time.sleep(RTM_READ_DELAY)
    else:
        print("Connection failed. Exception traceback printed above.")


def parse_wizard_commands(slack_events):
    '''
    Filter on events that are directed at the wizard
    '''

    wizard_commands = []

    for event in slack_events:
        if event["type"] == "message" and not "subtype" in event:

            message_text = event["text"]
            print(message_text)
            if mentions_wizard(message_text):
                print("mention detected")
                wizard_commands.append(event)
            else:
                print("no mention")

    return wizard_commands

def mentions_wizard(message_text):
    match = re.search(MENTION_REGEX, message_text)
    wizard_id = config['wizard_id']

    if match:
        substring = match.group(0)
        if wizard_id in substring:
            print('WZZARD')
        else:
            print("nonwizzard %s doesn't match %s" % (part, wizard_id))
    return match

# def parse_wizard_mentions(slack_events):
#     '''
#     Filter on non-command mentions of the wizard
#     '''

#     wizard_mentions = []

#     for event in slack_events:
#         if event["type"] == "message" and not "subtype" in event:
#             pass

#     return wizard_mentions

# def parse_bot_commands(slack_events):
#     """
#         Parses a list of events coming from the Slack RTM API to find bot commands.
#         If a bot command is found, this function returns a tuple of command and channel.
#         If its not found, then this function returns None, None.
#     """
#     for event in slack_events:
#         if event["type"] == "message" and not "subtype" in event:



#                 return message, event["channel"]
#     return None, None




def handle_command(command, channel):
    """
        Executes bot command if the command is known
    """
    # Default response is help text for the user
    default_response = "Not sure what you mean. Try *{}*.".format(EXAMPLE_COMMAND)

    # Finds and executes the given command, filling in response
    response = None
    # This is where you start to implement more commands!
    if command.startswith(CLAIM_TOKEN):
        response = "Sure...write some more code then I can do that!"

    # Sends the response back to the channel
    slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        as_user = True,
        text=response or default_response
    )

if __name__ == "__main__":

    main()